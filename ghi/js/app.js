function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
        <div class="col">
            <div class="card shadow p-3 mb-5 bg-body-tertiary rounded">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <h6 class="card-subtitle mb-2 text-body-secondary text-secondary">${location}</h6>
                    <p class="card-text">${description}</p>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">${starts} - ${ends}</li>
            </div>
        </div>
    `;
}

function createAlert(message){
    return `
        <div class="alert alert-primary" role="alert">
            ${message}
        </div>`
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            const column = document.querySelector('.row');
            column.innerHTML += createAlert(`Error: ${response.status} - ${response.statusText}`);
        } else {
            const data = await response.json();
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const location = details.conference.location.name;
                    const pictureUrl = details.conference.location.picture_url;
                    const starts = new Date(details.conference.starts).toLocaleDateString();
                    const ends = new Date(details.conference.ends).toLocaleDateString();
                    const html = createCard(title, description, pictureUrl, starts, ends, location);
                    const column = document.querySelector('.row');
                    column.innerHTML += html;
                    console.log(html);
                }
            }
        }
    } catch (e) {
        console.error(`Error: ${e.message}`)
    }

});
