import { useEffect, useState } from "react"

function ConferenceForm() {

    const initialData = {
        name: '',
        starts: '',
        ends: '',
        description: '',
        max_presentations: '',
        max_attendees: '',
        location: '',
    }

    const [formData, setFormData] = useState(initialData)
    const [locations, setLocations] = useState([])

    const handleFormChange = (event) => {
        const key = event.target.name
        const value = event.target.value
        setFormData({
            ...formData,
            [key]: value,
        })
        
    }

    const fetchLocations = async () => {
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json()
            setLocations(data.locations)
        }
    }

    useEffect(() => {
        fetchLocations();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        console.log(formData);

        const locationUrl = 'http://localhost:8000/api/conferences/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newLocation = await response.json();
            setFormData(initialData)
        }
    }




    return (
        <div className="row">
            <div className="offset-2 col-8">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input value={formData.name} onChange={handleFormChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.starts} onChange={handleFormChange} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" />
                                <label htmlFor="starts">Starts</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.end} onChange={handleFormChange} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" />
                                <label htmlFor="ends">Ends</label>
                        </div>
                        <div className="form mb-3">
                            <label value={formData.description} onChange={handleFormChange} htmlFor="description">Descritpion</label>
                            <textarea className="form-control" name="description" id="description" rows="3"></textarea>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.max_presentations} onChange={handleFormChange} placeholder="Maximum presentations" required name="max_presentations" type="number" id="max_presentations"
                                className="form-control" />
                                <label htmlFor="max_presentations">Maximum presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.max_attendees} onChange={handleFormChange} placeholder="Maximum attendees" required name="max_attendees" type="number" id="max_attendees"
                                className="form-control" />
                                <label htmlFor="max_attendees">Maximum attendees</label>
                        </div>
                        <div className="mb-3">
                            <select value={formData.location} onChange={handleFormChange} required name="location" id="location" className="form-select">
                                <option value="">Choose a location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.id} value={location.id}>
                                            {location.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
export default ConferenceForm