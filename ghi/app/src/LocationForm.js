import React, { useEffect, useState } from "react";

function LocationForm() {
    
    // initalizes an empty Form
    const initialData = {
        name: '',
        room_count: '',
        city: '',
        state: '',
    };

    // creates for data and sets it accordiingto our initalized data
    const [formData, setFormData] = useState(initialData)

    
    const [states, setStates] = useState([]);

    const handleSubmit = async (event) => {
        event.preventDefault();

        console.log(formData);

        //set destination of data
        const locationUrl = 'http://localhost:8000/api/locations/';

        //configure fetch 
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        //send data using destination and configuration
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newLocation = await response.json();
            console.log(newLocation);
            //reset form to blank
            setFormData(initialData)
        }
    }

    const handleFormChange = (event) => {
        const key = event.target.name
        const value = event.target.value
        setFormData ({
            ...formData,
            [key]:value
        })
    }

    // -----INDIVIDUAL WAY OF SETTING VALUES------

    // const handleNameChange = (event) => {
    //     const value = event.target.value;
    //     setName(value);
    // }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/states/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setStates(data.states)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new location</h1>
                    <form onSubmit={handleSubmit} id="create-location-form">
                        <div className="form-floating mb-3">
                            <input value={formData.name} onChange={handleFormChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.room_count} onChange={handleFormChange} placeholder="Room count" required name="room_count" type="number" id="room_count"
                                className="form-control" />
                            <label htmlFor="room_count">Room count</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.city} onChange={handleFormChange} placeholder="City" required name="city" type="text" id="city" className="form-control" />
                            <label htmlFor="city">City</label>
                        </div>
                        <div className="mb-3">
                            <select value={formData.state} onChange={handleFormChange} required name="state" id="state" className="form-select">
                                <option value="">Choose a state</option>
                                {states.map(state => {
                                    return (
                                        <option key={state.abbreviation} value={state.abbreviation}>
                                            {state.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default LocationForm;