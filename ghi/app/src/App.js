import Nav from "./Nav";
import AttendeesList from "./AttendeesList";
import LocationForm from "./LocationForm";
import ConferenceForm from "./ConferenceForm"
import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";

function App(props) {

  return (
    <BrowserRouter>
      <Nav />
        <div className="container">
          <Routes>
            <Route index element={<MainPage/>} />

            <Route path='conferences'>
              <Route path="new" element={<ConferenceForm />} />
            </Route>

            <Route path='locations'>
              <Route path='new' element={<LocationForm />} />
            </Route>

            <Route path='attendees' element={ <AttendeesList /> } />

          </Routes>
          
          
        </div>
    </BrowserRouter>
  );
}

export default App;
